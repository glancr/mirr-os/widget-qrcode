# frozen_string_literal: true

module Qrcode
  class Engine < ::Rails::Engine
    isolate_namespace Qrcode
    config.generators.api_only = true
  end
end
