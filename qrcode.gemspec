# frozen_string_literal: true

require 'json'
$LOAD_PATH.push File.expand_path('lib', __dir__)

# Maintain your gem's version:
require 'qrcode/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name = 'qrcode'
  spec.version = Qrcode::VERSION
  spec.authors = ['Tobias Grasse']
  spec.email = ['tg@glancr.de']
  spec.homepage = 'https://glancr.de'
  spec.summary = 'mirr.OS widget that lets you show a QR code on your smart mirror.'
  spec.description = 'Shows a QR code for your URL, phone number or any custom text.'
  spec.license = 'MIT'
  spec.metadata = { 'json' =>
                      {
                        type: 'widgets',
                        title: {
                          enGb: 'QR Code',
                          deDe: 'QR-Code'
                        },
                        description: {
                          enGb: spec.description,
                          deDe: 'Zeigt einen QR-Code für deine URL, Telefonnummer oder beliebigen Text an.',
                          frFr: 'Affiche un code QR pour votre URL, votre numéro de téléphone ou tout autre texte personnalisé.',
                          esEs: 'Muestra un código QR para su URL, número de teléfono o cualquier texto personalizado.',
                          plPl: 'Pokazuje kod QR dla Twojego adresu URL, numeru telefonu lub dowolnego niestandardowego tekstu.',
                          koKr: 'URL, 전화 번호 또는 사용자 정의 텍스트에 대한 QR 코드를 표시합니다.'
                        },
                        sizes: [
                          { w: 2, h: 2 }
                        ],
                        group: nil,
                        compatibility: '0.1.0',
                        single_source: false
                      }.to_json }

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = 'http://gems.marco-roth.ch'
  else
    raise 'RubyGems 2.0 or newer is required to protect against ' \
      'public gem pushes.'
  end

  spec.files = Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.md']

  spec.add_development_dependency 'rails', '~> 5.2'
  spec.add_development_dependency 'rubocop'
  spec.add_development_dependency 'rubocop-rails'
end
