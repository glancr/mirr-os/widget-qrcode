# frozen_string_literal: true

module Qrcode
  class Configuration < WidgetInstanceConfiguration
      attribute :custom_input, :boolean, default: false
      attribute :input_type, :string, default: "url"
      attribute :content, :string, default: ""

      validates :custom_input, boolean: true
      validates :input_type, inclusion: %w(url text tel)
  end
end
